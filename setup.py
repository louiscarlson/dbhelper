from setuptools import setup

setup(
    name = 'dbhelper',
    version = '1.0',
    description = 'simplifies and speeds the creation and reuse of connections. good for threads.',
    author = 'T. What',
    author_email = 'thinkywhatsit@gmail.com',
    packages = ['dbhelper'],
    install_requires=[
                "util"
        ]
    )