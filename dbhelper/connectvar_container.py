"""
defines ConnectvarContainer class for use in the login class or the manual_connect class

"""

class ConnectvarContainer():
    """ class for connection variables
    instantiated by the login class or the manual_connect class
    """

    def __init__(self, user, host, password, database, port=3306):
        """
        :param user: username for the database connection
        :param host: hostname or IP for the database connection
        :param password: password for the database connection
        :param database: schema name for the connection
        :param port: port number, defaults to 3306
        """
        self.user = user
        self.host = host
        self.password = password
        self.database = database
        self.port = port

    def return_vars_as_dict(self):
        """
        :return: a dict of all of the connection properties
"""
        out = {}
        out['user'] = self.user
        out['host'] = self.host
        out['password'] = self.password
        out['database'] = self.database
        out['port'] = self.port
        return out
