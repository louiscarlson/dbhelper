"""

defines Login class for use in SwitchBoard class

"""
import time
import hashlib # create a hash of host and username for pooled connections
from configparser import ConfigParser
from dbhelper.connectvar_container import ConnectvarContainer
from dbhelper.connection_broker import ConnectionBroker

class Login():

    """ the Login class is a discrete set of connection variables that comprise a login
    for instance
      same host, port, username, password and server
      = same Login

"""

    def __init__(self, switchboard_request):
        """ an object representing a single set of connection parameters for a host
        don't call this __init__ directly unless you're nuts.
        instead, call the switchboard
"""
        # even though a login can apply to many config_db_names,
        # this is the one for which it was first created
        self.config_db_name = switchboard_request.config_db_name

        if switchboard_request.manual_connect is not None:
            # read this from the input rather than the config
            self.connect_var_container = switchboard_request.manual_connect
        else:
            self.config_file = switchboard_request.config_file
            self.config_file_vars = {}
            self.read_config()
            self.connect_var_container = ConnectvarContainer(
                user=self.config_file_vars['user'],
                host=self.config_file_vars['host'],
                password=self.config_file_vars['password'],
                database=self.config_file_vars['database'],
                port=self.config_file_vars['port']
                )
        # list of all databases
        self.databases = []
        # add it to the list
        self.add_database(self.connect_var_container.database)
        self.hash = None
        self.shorthash = None # used for the pool name
        self.make_hash()
        # references to the pool(s) available to this database_name
        self.pooled_connects = {}
        # references to the existing connections for this database_name
        self.non_pooled_connects = []
        # low rent locking for the process of adding a new thread
        self.adding_pool = False

        # pprint.pprint(vars(self))

    def add_nonpooled_connect(self, connection):
        """adds a non-pooled connection to the login container

        """
        self.non_pooled_connects.append(connection)

    def add_pooled_connect(self, connection, pool_name=None):
        """adds a pooled connection to the login container
        """
        if pool_name is not None:
            pool_name = pool_name
        else:
            pool_name = self.shorthash
        self.pooled_connects[pool_name] = connection
        # print(self.pooled_connects)

    def make_connect(self, switchboard_request, add_pool=False):
        """ this function creates a ConnectionBroker and establishes
        a new connection to the db when one is needed, usually
        when no prior connection has been established for the login
        but can also be used when a pooled connection is requested but
        the pool has no more connections available
        """
        this_connection_broker = ConnectionBroker(switchboard_request)
        if add_pool:
            # forcing a new pool to open, so use a new pool name
            use_pool_name = switchboard_request.database_name.login.shorthash[:-4]\
            + '_' + str(len(self.pooled_connects) + 1)
        else:
            use_pool_name = switchboard_request.database_name.login.shorthash

        if add_pool:
            while self.adding_pool:
                # force this thread to wait 1 millisecond and try again
                time.sleep(1)
                # print("waiting one...")

            self.adding_pool = True

        this_connection = this_connection_broker.make_connection(use_pool_name)

        if self.adding_pool:
            # turn this back off so the next thread waiting can go
            self.adding_pool = False

        if switchboard_request.use_pool:
            self.add_pooled_connect(this_connection, pool_name=use_pool_name)
        else:
            self.add_nonpooled_connect(this_connection)

        return this_connection

    def return_connection(self, switchboard_request):
        """ checks the list of connections for the pool type, creates one if needed,
        and returns a connection

        when called for non-pooled connections, calls make_connect()
        """

        if not switchboard_request.use_pool: # looking for a non pooled connection
            if not self.non_pooled_connects: # no connection has yet been made
                # MySQLConn(this_database_name)
                this_connection = self.make_connect(switchboard_request)
            elif self.non_pooled_connects: # at least one connection exists
                for npc in self.non_pooled_connects:
                    if not npc.in_transaction:
                        this_connection = npc
                if not this_connection: # didn't find one, make a new one
                    this_connection = self.make_connect(switchboard_request)
        else: # pooled connection
            if not self.pooled_connects:
                # no pooled connection for this login yet
                this_connection = self.make_connect(switchboard_request)
                # pprint.pprint(self.pooled_connects)
            else: # pools exist. try to get a connection from one
                # changed this to a list as it was breaking when the dict
                # size would change during iteration
                for pn in list(self.pooled_connects):
                    # try all of the existing pools first
                    this_connection = self.try_pool(pool_name=pn)
                    if this_connection is not None:
                        # stop trying when you find one
                        break

                if this_connection is None:
                    # still didn't find a connection from any of the open pools, so force a new one
                    this_connection = self.make_connect(switchboard_request, add_pool=True)


        return this_connection


    def try_pool(self, pool_name):
        """ exists to handle pool exhausted errors
        """
        # print("trying to get pooled connection")
        out_connection = None
        try:
            out_connection = mysql.connector.connect(pool_name=pool_name)
        # except (mysql.connector.errors.PoolError, queue.Empty) as err:
        except:
            #print("failed to get connection")
            #pprint.pprint(vars(err))

            pass

        return out_connection


    def make_hash(self):
        """ creates a hash from connection information for the login
        to be used later when comparing a new connection request against
        existing logins
        """
        this_hash_contents = self.connect_var_container.host\
        + self.connect_var_container.user + self.connect_var_container.password\
        + str(self.connect_var_container.port)
        this_hash = hashlib.new("sha512")
        this_hash.update(this_hash_contents.encode("utf-8"))
        self.hash = this_hash.hexdigest()
        self.shorthash = self.hash[:64] # used for the pool name

    def read_config(self):
        """reads in the config file and creates a login object from it
"""
        parser = ConfigParser()
        parser.read(self.config_file)
        if parser.has_section(self.config_db_name):
            items = parser.items(self.config_db_name)
            for item in items:
                self.config_file_vars[item[0]] = item[1]
        else:
            raise ValueError('{0} not found in the {1} file'.format(section, filename))

        if 'port' not in self.config_file_vars:
            self.config_file_vars['port'] = 3306

    def add_database(self, database):
        """ checks the list of known schemas for the login and adds one
        if it isn't already in the list
"""
        if database not in self.databases:
            self.databases.append(database)
