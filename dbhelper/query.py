"""defines the Query object used by dbhelper to define and run queries"""

import time
from decimal import *

class Query():
    """Runs a query against a given DB instance. Requires that the DB credentials are
    loaded in a file outside of the calling script or class."""
    def __init__(self, switchboard_request):
        # used later
        self.results = []
        self.record_count = None
        self.execution_time = 0.000
        self.result_columns = []
        self.rows_affected = 0
        self.start_time = 0
        self.end_time = 0
        # current query
        self.query_text = switchboard_request.query_text
        self.proc_args = switchboard_request.proc_args
        self.query_type = switchboard_request.query_type
        self.escape_single_quotes = switchboard_request.escape_single_quotes
        self.connection = switchboard_request.connection

        if self.query_text is not None:
            if self.query_type == 'PROC':
                self.execute_proc()
            else:
                self.execute_query()

        self.execution_time = self.end_time - self.start_time

    def do_escape_single_quotes(self):
        """ change out any single quotes which may have been in the input data
"""
        self.query_text.replace("\'", "\\'")

    def execute_query(self):
        """executes a query against a given connection"""
        cursor = self.connection.cursor(buffered=True, dictionary=True)
        #set_unicode(cursor, False)
        if self.escape_single_quotes:
            self.do_escape_single_quotes()
        self.start_time = time.perf_counter()

        cursor.execute(self.query_text)

        self.end_time = time.perf_counter()
        self.rows_affected = cursor.rowcount
        # No recordset for INSERT, UPDATE, CREATE, etc
        # handle updates, inserts, deletes, etc which don't have a description or a recordset
        if cursor.description is not None:
            self.results = cursor.fetchall()
            self.result_columns = cursor.column_names

    def execute_proc(self):
        """executes a stored procedure"""
        cursor = self.connection.cursor(buffered=True, dictionary=True)
        #set_unicode(cursor, False)
        self.start_time = time.perf_counter()
        cursor.callproc(self.query_text, self.proc_args)
        self.end_time = time.perf_counter()

        counter = 0

        for x in cursor.stored_results():
            #only do this once
            if counter == 0:
                self.result_columns = x.column_names
            for y in x:
                self.results.append(dict(zip(x.column_names, y)))

        counter += 1

    def dump(self, to_HTML=False, to_var=False, include_query_info=False, total_columns=None, calculated=None):
        """dumps the results of the query to plain text or html\

        total_columns = a py list of column names which should be totaled up

        calculated = a dict of columns where the totals are calculated containing:
            key = the column name
                formula = the math by which the column should be calculated, in the format column name operator
                    other column(s)
                format = the format in which the resulting value should be output

        """
        
        if not total_columns:
            total_columns = []

        if not calculated:
            calculated = {}

        # hold the totals, if any
        totals = {}

        start_line = ""
        end_line = ""
        header = ""
        table_open = ""
        table_close = ""

        output_var = []
        numeric_class = ' class="numeric_col"'
        number_format = "{:,.2f}"
        int_format = "{:,}"
        percentage_format = "{:3.2f}%"

        if to_HTML:     
            start_line = "<tr>"
            end_line = "</tr>"
            table_open = "<table>"
            table_close = "</table>"

        if include_query_info:
            output_var.append(self.query_text)
            output_var.append("{:.4f}".format(self.execution_time) + ' seconds')

        if self.results:

            if include_query_info:
                output_var.append('records found:' + str(len(self.results)))
            
            output_var.append(table_open)
            counter = 0
            #print(self.results)
            each_line = ""
            # one common row format for all rows.
            for z in self.result_columns:
                if to_HTML:
                    header += "\t<th>" + str(z) + "</th>"
            
            output_var.append(start_line + header + end_line)

            # now format the values for each row and print them
            for x in self.results:
                # result_columns to honor the column order
                # using x.values() spits them out in the order of the dict,
                # which isn't the same as the column order
                # this_row_values = []
                this_line = []

                if to_HTML:
                    if counter % 2 == 0:
                        start_line = '<tr class="even_row">'
                    else:
                        start_line = '<tr class="odd_row">'

                this_line.append(start_line)

                for column_name in self.result_columns:

                    if column_name in total_columns and type(x[column_name]) in [int, float, Decimal]:
                        # don't try to total text or date cols
                        if column_name not in totals:
                            totals[column_name] = 0
                        totals[column_name] += x[column_name]

                    if to_HTML:
                        if type(x[column_name]) in [int, float, Decimal]:
                            # print(column_name, x[column_name], type(x[column_name]))
                            td_class = numeric_class
                            if type(x[column_name]) is int or x[column_name] % 1 == 0:
                                this_value = int_format.format(x[column_name])
                            else:
                                this_value = number_format.format(x[column_name])
                        else:
                            if column_name[-2:] == '_%':
                                td_class = numeric_class
                            else:
                                td_class = ''
                            
                            if type(x[column_name]) is bytearray:
                                this_value = x[column_name].decode()
                            else:
                                this_value = str(x[column_name])             

                        this_line.append("\t\t<td" + td_class + ">" + this_value + "</td>")
                    # plain text 
                    else:
                        this_line.append("\t\t" + str(column_name) + ": " + str(x[column_name]))

                this_line.append(end_line)

                output_var.append(''.join(this_line))

                counter += 1
            
            if totals or calculated:
                this_line = []
                total_start_line = '<tr class="total_row">'
                this_line.append(total_start_line)

                if calculated:
                    for calc_field in calculated.keys():
                        # change the format from column names to dict keys
                        for col_name in sorted(self.result_columns, key=len):
                            replace_value = r"totals['" + col_name + "']"
                            find_value = r'{' + col_name + '}'
                            calculated[calc_field]["formula"] = calculated[calc_field]["formula"].replace(find_value, replace_value)
                        try:
                            calculated[calc_field]['value'] = eval(calculated[calc_field]["formula"])
                            if calculated[calc_field]['format'] is 'int':
                                calculated[calc_field]['value'] = int_format.format(calculated[calc_field]['value'])
                            if calculated[calc_field]['format'] is 'number':
                                calculated[calc_field]['value'] = number_format.format(calculated[calc_field]['value'])
                            if calculated[calc_field]['format'] is 'percentage':
                                calculated[calc_field]['value'] = percentage_format.format(calculated[calc_field]['value'])
                        except:
                            calculated[calc_field]['value'] = ""
                            print(calculated[calc_field]["formula"])
                            # print("Unexpected error:", sys.exc_info()[0])
                            pass


                # we have at least one column, so iterate through again
                for column_name in self.result_columns:
                    if to_HTML:
                        if column_name in totals:
                            this_value = number_format.format(totals[column_name])            
                            this_line.append("\t\t<td" + numeric_class + ">" + this_value + "</td>")
                        elif column_name in calculated.keys():
                            # this column total will be calculated
                            this_value = calculated[column_name]['value']       
                            this_line.append("\t\t<td" + numeric_class + ">" + this_value + "</td>")
                        else:
                        # didn't total this one
                            this_line.append("\t\t<td></td>")

                    # plain text 
                    else:
                        if column_name in totals:
                            this_line.append("\t\t" + str(column_name) + ": " + str(totals[column_name]))
                        if column_name in calculated.keys():
                            this_line.append("\t\t" + str(column_name) + ": " + str(calculated[column_name]['value']))
                        else:
                            this_line.append("\t\t\t\t")

                this_line.append(end_line)

                output_var.append(''.join(this_line))

            output_var.append(table_close)

        elif include_query_info and self.rows_affected and self.rows_affected > 0:
            output_var.append(str(self.rows_affected) + ' rows affected.')
        else:
            if include_query_info:
                output_var.append('0 rows affected')

        output_text = "\n".join(output_var)

        if not to_var:
            print(output_text)

        if to_var:
            return output_text

        return None
