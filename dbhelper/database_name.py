"""
defines DatabaseName class to be used by the SwitchBoard

"""

from dbhelper.login import Login


class DatabaseName():
    """ class that keeps references to CONFIG_DB_NAME values
    as stored in a config file with a lookup to the single
    login classes that they contain.
    many database_names may reference the same login. a separate lookup of those is kept
    """
    def __init__(self, switchboard_request):
        """
        :param switchboard_request: a switchboard request object"""
        self.config_db_name = switchboard_request.config_db_name
        self.config_file = switchboard_request.config_file
        # set up a login for this CONFIG_DB_NAME which may be changed later
        self.login = Login(switchboard_request)
        self.database = self.login.connect_var_container.database
