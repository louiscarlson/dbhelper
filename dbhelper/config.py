
"""
configuration details for this installation of dbhelper
modify these to suit your environment

"""

"""
CONFIG_DB_NAME
        the initial database for a connection
        must exist in the config file listed below
"""
CONFIG_DB_NAME = 'tw'

"""
CONFIG_FILE
        the file with the connection info config
"""
CONFIG_FILE = 'c:\\etc\\config.ini'
