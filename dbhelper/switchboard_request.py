"""defines the SwitchBoardRequest object which defines the properties and
objects used to create connections and run queries"""
from dbhelper import config

class SwitchBoardRequest():
    """ object containing information about a switchboard request, largely the
    properties of the connection"""
    def __init__(self, config_db_name=None, config_file=config.CONFIG_FILE,
                 query_text=None, query_type='PROC', proc_args=None,
                 escape_single_quotes=False, use_pool=True, pool_size=32,
                 autocommit=True, pool_reset_session=True,
                 manual_connect=None):
        self.config_db_name = config_db_name
        self.config_file = config_file
        self.manual_connect = manual_connect
        self.query_text = query_text
        self.query_type = query_type
        self.proc_args = proc_args
        if self.proc_args is None:
            self.proc_args = []
        self.escape_single_quotes = escape_single_quotes
        self.use_pool = use_pool
        self.pool_size = pool_size
        self.autocommit = autocommit
        self.pool_reset_session = pool_reset_session
        self.database_name = None
        # this will be populated with a value if a pool matching the request is found
        self.pool_name = None
        self.connection = None
        self.query = None

    def set_database_name(self, database_name):
        """ sets the database_name property for the SwitchBoardRequest """
        self.database_name = database_name

    def set_connection(self, connection):
        """ sets the connection object for the SwitchBoardRequest """
        self.connection = connection

    def set_query(self, query):
        """ sets the query object for the SwitchBoardRequest """
        self.query = query
