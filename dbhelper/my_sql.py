"""my_sql.py module defines the SwitchBoard() class and its methods for using
with a MySQL database server

see https://thinkywhatsit@bitbucket.org/thinkywhatsit/dbhelper.git
for details

"""
import util

from dbhelper.switchboard_request import SwitchBoardRequest
from dbhelper.connectvar_container import ConnectvarContainer
from dbhelper.database_name import DatabaseName
from dbhelper.login import Login
from dbhelper.query import Query


if __name__ == "__main__":
    import config
else: # assume package
    import dbhelper.config as config

class SwitchBoard():
    """ need a connection to a db? we got one. or we can make one. assuming
    your credentials are good. but that'd be your problem.
    pass in the details for the connection (see run_query()), and whether you're
    looking for a pooled or a standalone connection, and the switchboard
    will either return an available connection for the parameters specified
    or establish a new one

"""

    logins = {} # dict of all logins known to the switchboard
    """ logins can refer to many different schemas for a single db server
    important that logins and dataBaseNames vars are shared across all
    instantiations of SwitchBoard() as the point is to create fast,
    thread-safe re-use of the connections
"""

    database_names = {}
    """ dict of all config_db_names known to the switchboard
    with a pointer to the relevant login
    database_names is important because it's the names by which the code
    refers to the schema and database server in the config
    not to be confused with database or schema """

    def __init__(self):
        """ initializes an empty SwitchBoard object
"""

    def add_login(self, switchboard_request):
        """ adds a login object to the list of logins known to the SwitchBoard
"""
        if switchboard_request.database_name.login.hash not in self.logins:
            self.logins[switchboard_request.database_name.login.hash] =\
            switchboard_request.database_name.login

    def add_database_name(self, database_name):
        """adds a database name to the list of database_names
        known to the SwitchBoard"""
        self.database_names[database_name.config_db_name] = database_name

    def get_login(self, login):
        """ checks the list of known logins for a specific login using the hash value
returns the login object already known to the switchboard, or adds the new one and returns that
        """
        # print(login.hash)
        if login.hash not in self.logins:
            self.logins[login.hash] = login
        return self.logins[login.hash]

    def dump_logins(self):
        """ returns the known logins"""
        return self.logins

    def search_database_names(self, switchboard_request):
        """searches the list of database names known to thw SwitchBoard to see
        if one already exists with a given name, returns it if it does"""
        if switchboard_request.config_db_name in self.database_names:
            return self.database_names[switchboard_request.config_db_name]

        return None

    def search_logins(self, database_name):
        """searches the list of logins known to the SwitchBoard for a given
        database_name returns the login if one exists for the
        database_name"""
        if database_name.login.hash in self.logins:
            # we already have this login
            return self.logins[database_name.login.hash]

        return None

    def get_database_name(self, database_name):
        """ creates an entry in the CONFIG_DB_NAME dict with the relevant
        login and schema and returns a reference
        """
        if database_name.config_db_name not in self.database_names:
            self.database_names[database_name.config_db_name] = database_name

        return self.database_names[database_name.config_db_name]

    def run_query(self, config_db_name=None, query_text=None, query_type='PROC',
                  proc_args=None, escape_single_quotes=False, config_file=config.CONFIG_FILE,
                  use_pool=False, pool_size=32, autocommit=True, manual_connect=None):
        """ entry point for most switchboard uses
        creates a switchboard request then gets a connection according to the specified parameters
        then runs the query (or proc) against the specified connection
        and returns a query object
        example:
        x = MySQLdb(CONFIG_DB_NAME = 'someDB')
        x.run_query('select * from table', query_type = 'SQL')
        then read the results from x.results

        manual_connect is a dict of values to be used instead of a config file
        host : "value",
        database : "value",
        user : "value",
        password : "value",
        port : "value"

"""
        if proc_args is None:
            proc_args = []

        if manual_connect is not None:
            # set a different CONFIG_DB_NAME, as this one won't come from the
            # config file
            config_db_name = manual_connect.host + "_" + manual_connect.user +\
            "_" + str(manual_connect.port)

        this_switchboard_request = SwitchBoardRequest(config_db_name, config_file, query_text,
                                                      query_type, proc_args,
                                                      escape_single_quotes,
                                                      use_pool, pool_size,
                                                      autocommit,
                                                      manual_connect=manual_connect)

        this_switchboard_request.set_connection(self.request_connect(this_switchboard_request))

        # self.check_change_DB(CONFIG_DB_NAME)
        this_switchboard_request.connection.database =\
        this_switchboard_request.database_name.database

        if query_text is not None:
            this_switchboard_request.set_query(Query(this_switchboard_request))

        if this_switchboard_request.use_pool:
            # return the connection to the pool
            this_switchboard_request.connection.close()

        return this_switchboard_request.query


    def add_login_to_database(self, switchboard_request):
        """adds a Login object to a given database_name"""
        this_login = Login(switchboard_request)
        # print(switchboard_request.database_name)
        switchboard_request.database_name.login = this_login
        # print(switchboard_request.database_name)
        self.add_login(this_login)

    def request_connect(self, switchboard_request):
        """ checks the list of known database_names and logins to see if there's a match
returns a matched database_name if one exists, else creates a new one. same for logins
"""
        # test to see if this one already exists
        this_database_name = self.search_database_names(switchboard_request)

        if this_database_name is None: # does not exist
            this_database_name = DatabaseName(switchboard_request) # create a new database_name

            # before adding database_name, see if the login exists in that lookup
            this_login = self.search_logins(this_database_name)

            if this_login is None: # nope, not found, so the database_name will be authoritative

                # some login exists matching this database name
                if this_database_name.login is not None:
                    # assign the login for the database_name to this_login
                    this_login = this_database_name.login
                    # add the new login to the dict
                else: # this database_name has no login, need to make one
                    self.add_login_to_database(switchboard_request)
                    this_login = this_database_name.login

            else: # found this login already, so change the reference in the database_name
                this_database_name.login = this_login

            # now we should have a complete and correct database_name, so add it to the dict
            self.add_database_name(this_database_name)

        else: # found the database_name

            if this_database_name.login is not None:
                this_login = this_database_name.login
            else: # this database_name doesn't have a login yet. it happens
                self.add_login_to_database(switchboard_request)
                this_login = this_database_name.login

        # and set the database_name for the switchboard_request
        switchboard_request.set_database_name(this_database_name)

        # and make sure the login is in self.logins
        self.add_login(switchboard_request)

        this_connection = this_login.return_connection(switchboard_request)
        # pprint.pprint(this_connection)

        return this_connection



def manual_connect_input():
    """method for gathering manual input from the user to create a connection
    that is not in a named configuration file
    returns a connect_var_container()
    x = MySQL.SB.run_query(schemaName, query_text = [query text],
                           query_type = 'SQL', manual_connect = c)

    """

    this_user = util.getInput.getInput("string", "username", min=1)
    this_pass = util.getInput.getInput("string", "password", silent=True, min=1)
    this_host = util.getInput.getInput("string", "host", min=1, defaultVal="localhost")
    this_database = util.getInput.getInput("string", "database", min=1)
    this_port = util.getInput.getInput("int", "port", silent=True,
                                       defaultVal="3306",
                                       applyDefaultValueOnError=True)
    this_connection = ConnectvarContainer(user=this_user,
                                          password=this_pass,
                                          host=this_host,
                                          database=this_database,
                                          port=this_port)

    return this_connection


if 'config' in locals() and 'CONFIG_DB_NAME' in vars(config) and 'CONFIG_FILE' in vars(config):
    # we now have enough stuff to create a connection
    # db = MySQLdb(CONFIG_DB_NAME = config.CONFIG_DB_NAME, CONFIG_FILE = config.CONFIG_FILE)

    SB = SwitchBoard()
    # get a connection ready for use
    THIS_REQUEST = SwitchBoardRequest(config_db_name=config.CONFIG_DB_NAME,
                                      config_file=config.CONFIG_FILE)
