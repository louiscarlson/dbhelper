"""

defines ConnectionBroker class for use in other modules

"""

import copy
import mysql.connector

class ConnectionBroker():
    """creates a re-usable connection to the DB server

"""
    def __init__(self, switchboard_request):
        self.switchboard_request = switchboard_request

    def make_connection(self, use_pool_name=None):
        """use_pool_name specifies a name for the pool as requested by the login.
        usually the short_hash for the login but can be incremented when that pool
        comes up exhausted

        """
        # make a deep copy of this
        use_connect_vars = copy.copy(
            self.switchboard_request.database_name.login.connect_var_container.return_vars_as_dict()
            )
        if self.switchboard_request.use_pool:
            # set these additional parameters to create a pooled connection for threads and such
            use_connect_vars['pool_size'] = self.switchboard_request.pool_size
            # the login may request a new pool name if the pool is exhausted
            use_connect_vars['pool_name'] = use_pool_name
            use_connect_vars['pool_reset_session'] = self.switchboard_request.pool_reset_session

        this_connection = mysql.connector.connect(**use_connect_vars)

        this_connection.autocommit = self.switchboard_request.autocommit

        return this_connection
    